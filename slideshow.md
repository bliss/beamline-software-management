name: empty layout
layout: true

---
class: center, middle

Beamline software management proposal
-------------------------------------

Conda - Ansible - Supervisor
============================

[Tiago Coutinho](https://github.com/tiagocoutinho) - [Vincent Michel](https://github.com/vxgmichel)

16/11/2017 - ESRF

•

Gitlab: [bliss/beamline-software-management](https://gitlab.esrf.fr/bliss/beamline-software-management)

Slides: [gitlab pages](http://bliss.gitlab-pages.esrf.fr/beamline-software-management/)

---
class: middle

# Summary

* Motivation

* The 3 pillars

* Use case

* Ecosystem

* Building packages

* Discussion

---

class: center, middle

# Motivation

---

## Hard to kick start a bliss project

* ... oh, so many reasons!

<!-- Why?
* manage all bliss dependencies in blissbuilder (over 35 direct dependencies)
* configure bliss services (huge amount of admin/etc/S10beacon)
* manage bliss services
-->

--

## Open the door to new possibilities

  * Develop *clean* software

  * Develop safely on beamline

  * Patch quickly and safely

  * Install any version of any application on any PC

---

# Split problem in three classes

* Package management

* Configuration management

* Service management

---
# Principles

--

## **Focus** - solve the problem

--

## **Cooperation** - don't break existing installations

--

## **Simplicity** - Use existing tools

---
# Use case

### *a new pc is being installed on a beamline or...<br/> an old machine needs to be blissified*

#### 1. Add host name to the global configuration

#### 2. Run a script which will:

* base installion (conda, supervisor, ...)

* create and prepare sandboxed environment(s)

* install bliss

* make sure bliss services are up and running

* prompt the changes

---

class: center, middle

# The 3 pillars

.center[![banner](images/banner.png)]

---
class: center, middle

.center[![conda](images/conda.svg)]

•

## [Conda](http://conda.io) is both:

### an open source package management system

### an environment management system

•

### It is cross-platform

### It does not require admin rights

---
class: center, middle

# The conda ecosystem

### **conda CLI** - see cheat sheet

### **anaconda navigator** - a Qt GUI

### **anaconda cloud** - loads of extra packages

### **private conda channels** - for the BCU packages

---
class: center, middle

# demo time

---
class: center, middle

# ( Building conda packages )

### **Git repositories** - for the conda recipes

### **Gitlab CI** - to trigger the builds

### **Docker containers** - as controlled build environments

### **Lighttpd** - to serve the packages using http

---
class: center, middle

# Building conda packages

### Already more than 15 packages available

### **pytango**, **silx**, **lima**, **pymca**, etc.

### Two private channels

### [bcu-ci/stable](http://bcu-ci.esrf.fr/stable) • [bcu-ci/devel](http://bcu-ci.esrf.fr/devel)

---
class: center, middle
.center[![ansible](images/ansible.svg)]

•

## [Ansible](http://www.ansible.com) automates stuff

### configures systems and deploy software

### manages the machines in an agent-less manner

---
class: center, middle

# The ansible ecosystem

### ansible CLI

### Git repositories for storing the configuration

![ansible](images/ansible_graph.png)

---
class: center, middle

# demo time

[inventory](http://gitlab.esrf.fr/id31/ansible/blob/master/ansible/inventory)

[playbook](http://gitlab.esrf.fr/id31/ansible/blob/master/ansible/conda.yml)

---
class: center, middle

.center[![supervisor](images/supervisord.jpg)]

# [Supervisor](http://supervisord.org)

•

# [supervisor](http://supervisord.org) manages user services

## monitors and control services related to a project

---
class: center, middle

# The supervisor ecosystem

### **supervisor CLI** - supervisorctl

### **Basic GUI** - a Web interface

### **CESI** - Centralized Supervisor Interface

![cesi](images/cesi_graph.png)

---
class: center, middle

### (demo time)

[supervisor @ lid312](http://lid312.esrf.fr:9001)

[supervisor @ lid00c](http://lid00c.esrf.fr:9001)

[supervisor @ bcu01ctrl](http://bcu01ctrl.esrf.fr:9001)

[cesi](bcu01ctrl:5000)


---
class: center, middle

# **Open discussion**

---
class: center, middle

Beamline software management proposal
-------------------------------------

--

Round 2!
========

--

.center[![banner](images/banner.png)]


## Package  -  Configuration  -  Services

---
# Conda

## Already more than 20 packages available

qt3 (qt3, qub, qwt), tango (orb, pytango, tango), pymca, silx, **lima**

## [How-to build a conda package at ESRF](https://gitlab.esrf.fr/bliss/conda-recipes/#conda-recipes)

---

# Ansible - Roles

### Beamline PC (ex: Industrial PC)

* supervisor
* conda
* conda environment *bliss*
* bliss

### Beamline workstation

* **is a** *Beamline PC*
* **Beamline software**

### Beacon station

* **is a** *Beamline PC*
* beacon-server running
* multivisor running

---

# Beamline inventory

```yaml
...

[id31]
bibhelm
lid311
lid312
lid31bas1
lid31bas2

[id31:vars]
TANGO_HOST=bibhelm:20000
BEACON_HOST=bibhelm:25000
BEAMLINENAME=ID31

...
```

--

```bash
$ ansible-playbook bliss.yml
Which host would you like to setup ?
```

---

### 1. is machine prepared?

*No*: ask TID to create conda partition

*Yes*: make sure:

* miniconda is installed
* conda BCU channel is configured
* supervisor is installed and configured
* *blissenv* is installed
* *bliss* conda environment is created
* `bliss` python package is at latest master

### 2. is workstation?

1. **Beamline software** is prepared

### 3. is beacon station?

* **Beamline configuration** is prepared
* `beacon-server` is configured and running
* `multivisor` is configured and running

---

## Beamline configuration

.center[![beamline configuration](images/bl_config.svg)]

---

## Beamline software

.center[![beamline software](images/bl_soft.svg)]


---

## Beamline software installation

.center[![beamline software installation](images/bl_soft_install.svg)]

---

# Overview

.center[![summary](images/bl.svg)]

```python

$ . blissenv
$ bliss -s sixc

[SIXC]: from bliss.common.scans import mesh

[SIXC]: from id42.sequences import flash_scan

```

---

# How to steal from other beamlines

.center[![summary](images/bl_2.svg)]


```python

$ . blissenv
$ cd local
$ git clone git@gitlab:ID99/id99
$ pip install -e .
$ bliss -s sixc

[SIXC]: from id99.sequences import turbo_scan

```

---

# Supervisor

* ~~cesi~~
* multivisor

.center[![summary](images/multivisor.png)]

[Demo](http://bcu01ctrl:9002)