## Beamline software management proposal

# Conda - Ansible - Supervisor

***

### Introduction (t.)

### Presenting the 3 pillars:

- Conda - package management (v.)
- Ansible - configuration management (v.)
- Supervisor - service management (t.)

### In practice: (t.)

- a new pc is being installed on a beamline
  or an old machine needs to be blissified
- the hostname has to be added to the global ansible configuration
- we then run the ansible playbook, meaning ansible will:
   * install conda and supervisor
   * create the different conda environments
   * clone and install bliss
   * set the environments variables
   * make sure beacon supervisor service is running
   * prompt the changes (demo time!)

### The ecosystem:

- conda: (v.)
  * conda CLI (see cheat sheet)
  * anaconda navigator (demo time!)
  * anaconda cloud (for extra packages)
  * private anaconda channel (for bcu packages)
- ansible: (t.)
  * ansible CLI
  * Git repositories for storing the configuration
  * ansible tower / AWX web interface?
- supervisor: (t.)
  * supervisor CLI
  * Basic web interface
  * CESI: Centralized Supervisor Interface (maybe demo)

### Building packages: (v.)

- A git repo for the conda recipes
- Integration using gitlab ci (demo time!)
- The jobs run in a docker container
- Packages are published on private conda channels
- ~15 packages already available!

### Questions?
