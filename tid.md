Conda meeting with TID
======================

Adressing the following points:


## Impact of replacing blissinstaller with conda

- Blissintstaller is a package management system that does not interfer with system packages
- Conda is a package management system that does not interfer with system packages
- Hence, the two the systems can live next to each other during the transition


## Why BCU need a 20G /users/blissadm/conda directory not backed up?

- Up to 2G per environments (typical bliss env is 1.2G)
- Backup is not neccessary: because ansible


## What is in the directory tree?

- `/users/blissadm/conda`: root of the base conda env
- `/users/blissadm/conda/pkgs`: package cache
- `/users/blissadm/conda/envs`: default location for the environments
- `/users/blissadm/conda/bin`: location of the conda scripts (activate, deactivate, conda)


## How to create several conda environments on a Linux

Two tools available:
- Conda CLI
- Conda navigator GUI


## How to re-package software to be "conda" compatible

- Write a conda recipe
- Commit to a dedicated branch in bliss/conda-recipes
- Gitlab-ci fires up a docker container and run the builds
- Packages are copied in private conda channels
- The channels are served using lighttpd:
  * http://bcu-ci.esrf.fr/stable
  * http://bcu-ci.esrf.fr/devel


## The main advantage of conda

- Make all debian 6 to 9 machines look identical when it comes to installing BCU software
- BCU can stop supporting old version of dependencies because the newer versions are not available as system packages on old OSes
- Typically, python 2.6 -> python 2.7 (and potentially -> python 3.6 in the future)


## The case of the SSL ISPyB issue and its conda fix:

Problem:
  * ISPyB upgrade
  * Move to HTTPS
  * SSL issue because debian6 packages are too old

Fix:
  * A standard conda environment has been installed (using ansible?)
  * A conda environment for Mx3 has been created
  * The python interpreter comes with a more recent SSL modules which fixes the issue
  * Problem solved!

New problems arise... (lucid lib requires opencv2 which required a downgrade of numpy)


## About ansible and supervisors

See the slides.
